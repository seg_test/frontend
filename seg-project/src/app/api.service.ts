import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { catchError, tap, map } from 'rxjs/operators';
import { User } from './user';
import { Order } from './order';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};
const apiUrl = "//localhost:1337";
const usersUrl = apiUrl + "/users";
const ordersUrl = apiUrl + "/orders";

@Injectable({
  providedIn: 'root'
})

export class ApiService {

  constructor(private http: HttpClient) { }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  getUsers (): Observable<User[]> {

    return this.http.get<User[]>(usersUrl)
      .pipe(
        tap(heroes => console.log('fetched users')),
        catchError(this.handleError('getUsers', []))
      );
  }

  getUser(id: string): Observable<User> {

    const url = `${usersUrl}/${id}`;
    return this.http.get<User>(url).pipe(
        tap(_ => console.log(`fetched user id=${id}`)),
        catchError(this.handleError<User>(`getUser id=${id}`))
    );
  }

  getUserLogin(userName: string): Observable<User> {

    const url = `${usersUrl}?name=${userName}`;
    return this.http.get<User>(url).pipe(
        tap(_ => console.log(`fetched user id=${userName}`)),
        catchError(this.handleError<User>(`getUserLogin id=${userName}`))
    );
  }

  addUser(user): Observable<User> {

    return this.http.post<User>(usersUrl, user, httpOptions).pipe(
        tap((user: User) => console.log(`added user w/ id=${user.id}`)),
        catchError(this.handleError<User>('addUser'))
    );
  }

  updateUser (id, user): Observable<any> {

    const url = `${usersUrl}/${id}`;
    return this.http.put(url, user, httpOptions).pipe(
        tap(_ => console.log(`updated user id=${id}`)),
        catchError(this.handleError<any>('updateUser'))
    );
  }

  deleteUser (id): Observable<User> {

    const url = `${usersUrl}/${id}`;
    return this.http.delete<User>(url, httpOptions).pipe(
        tap(_ => console.log(`deleted user id=${id}`)),
        catchError(this.handleError<User>('deleteUser'))
    );
  }

  getOrders (userId: string): Observable<Order[]> {

    let _ordersUrl = `${ordersUrl}`;
    if (userId != ''){
        _ordersUrl = `${ordersUrl}?userId=${userId}`;
    }
    return this.http.get<Order[]>(_ordersUrl)
      .pipe(
        tap(heroes => console.log('fetched orders')),
        catchError(this.handleError('getOrders', []))
      );
  }

  getOrdersAgg (userId: string): Observable<Order[]> {

    let _ordersUrl = `${ordersUrl}/aggregation`;
    if (userId != ''){
        _ordersUrl = `${ordersUrl}/aggregationByUserId?userId=${userId}`;
    }
    return this.http.get<Order[]>(_ordersUrl)
      .pipe(
        tap(heroes => console.log('fetched orders')),
        catchError(this.handleError('getOrdersAgg', []))
      );
  }

  getOrder(id: string): Observable<Order> {

    const url = `${ordersUrl}/${id}`;
    return this.http.get<Order>(url).pipe(
        tap(_ => console.log(`fetched order id=${id}`)),
        catchError(this.handleError<Order>(`getOrder id=${id}`))
    );
  }

  addOrder(order): Observable<Order> {

    return this.http.post<Order>(ordersUrl, order, httpOptions).pipe(
        tap((order: Order) => console.log(`added order w/ id=${order.id}`)),
        catchError(this.handleError<Order>('addOrder'))
    );
  }

  updateOrder (id, order): Observable<any> {

    const url = `${ordersUrl}/${id}`;
    return this.http.put(url, order, httpOptions).pipe(
        tap(_ => console.log(`updated order id=${id}`)),
        catchError(this.handleError<any>('updateOrder'))
    );
  }

  deleteOrder (id): Observable<Order> {

    const url = `${ordersUrl}/${id}`;
    return this.http.delete<Order>(url, httpOptions).pipe(
        tap(_ => console.log(`deleted order id=${id}`)),
        catchError(this.handleError<Order>('deleteOrder'))
    );
  }
}
