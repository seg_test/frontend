import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent }   from './login/login.component';
import { HomeComponent }   from './home/home.component';
import { CustomersComponent }   from './customers/customers.component';
import { OrdersComponent }   from './orders/orders.component';


const routes: Routes = [
    {
        path: '',
        children: [
            { path: '', redirectTo: 'login', pathMatch: 'full' },
            { path: 'login', component: LoginComponent },
            { path: 'home', component: HomeComponent },
            { path: 'home/:userId', component: HomeComponent },
            { path: 'customers', component: CustomersComponent },
            { path: 'orders', component: OrdersComponent },
            { path: 'orders/:userId', component: OrdersComponent },
            { path: '**', redirectTo: 'login', pathMatch: 'full' },
        ]
    }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
