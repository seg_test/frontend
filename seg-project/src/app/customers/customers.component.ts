import { Component, ViewChild, Inject} from '@angular/core';
import { RouterModule, ParamMap, ActivatedRoute, Router } from '@angular/router';
import { MatMenuTrigger, MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ApiService } from '../api.service';
import { User } from '../user';

@Component({
    selector: 'app-customers',
    templateUrl: './customers.component.html',
    styleUrls: ['./customers.component.css']
})

export class CustomersComponent {

    constructor(public dialog: MatDialog,
        private api: ApiService,
        private route: ActivatedRoute,
        private router: Router) { }

    displayedColumns: string[] = ['id', 'name', 'type', 'maxOrders', 'creationDate', 'more'];
    dataSource: User[] = [];

    ngOnInit() {

      this.api.getUsers()
        .subscribe(res => {
            this.dataSource = res;
            console.log(this.dataSource);
        }, err => {
            console.log(err);
        });
    }

    openDialog(item: User): void {

        const dialogRef = this.dialog.open(CustomerDialog, {
            width: '500px',
            data: item
        });

        dialogRef.afterClosed().subscribe(result => {
            console.log('The customer dialog was closed');
            if (!result){
              return;
            }
            console.log(result);
            if (result.mode == 'Edit'){
                this.api.updateUser(result.id, result)
                    .subscribe(res => {
                        this.ngOnInit();
                    }, err => {
                        console.log(err);
                    });
            }
            else if (result.mode == 'Add'){
                var today = new Date();
                var dd = String(today.getDate()).padStart(2, '0');
                var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
                var yyyy = today.getFullYear();
                result.creationDate = yyyy + '-' + mm + '-' + dd;
                console.log(result);
                this.api.addUser(result)
                    .subscribe(res => {
                        this.ngOnInit();
                    }, err => {
                        console.log(err);
                    });
            }
        });
    }

    @ViewChild(MatMenuTrigger)
    contextMenu: MatMenuTrigger;

    contextMenuPosition = { x: '1000px', y: '1000px' };

    onContextMenu(event: MouseEvent, element: Element) {

         event.preventDefault();
         this.contextMenuPosition.x = event.clientX + 'px';
         this.contextMenuPosition.y = event.clientY + 'px';
         this.contextMenu.menuData = { 'item': element };
         this.contextMenu.openMenu();
     }

     onAdd(){

        console.log(`Click on Add`);
        let item: User = {};
        item.mode = 'Add';
        this.openDialog(item)
     }

     onContextMenuEdit(item: User) {

        console.log(`Click on Edit for ${item.id}`);
        item.mode = 'Edit';
        this.openDialog(item)
     }

     onContextMenuDelete(item: User) {

        console.log(`Click on Delete for ${item.id}`);
        this.api.deleteUser(item.id)
            .subscribe(res => {
                this.ngOnInit();
            }, err => {
                console.log(err);
            });
     }

     onUserSelect(element: User) {

        if (element.type == "Admin"){
            return;
        }
        this.router.navigate(['/orders', element.id]);
     }
}

@Component({
  selector: 'customer-dialog',
  templateUrl: './customer-dialog.html',
})

export class CustomerDialog {

    constructor(
        public dialogRef: MatDialogRef<CustomerDialog>,
        @Inject(MAT_DIALOG_DATA) public data: User) {}

    onCancelClick(): void {
        this.dialogRef.close();
    }
}
