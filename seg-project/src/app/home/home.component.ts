import { Component, OnInit } from '@angular/core';
import { RouterModule, ParamMap, ActivatedRoute, Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { ApiService } from '../api.service';
import { Chart } from 'angular-highcharts';
import * as Highcharts from 'highcharts';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

    chart: Chart;
    constructor(private api: ApiService,
        private route: ActivatedRoute,
        private router: Router,
        private cookieService: CookieService) { }

    userId = this.route.snapshot.paramMap.get('userId') || '';
    userIdC: string;
    userTypeC: string;

    ngOnInit() {

        this.userIdC = this.cookieService.get('segUserId');
        this.userTypeC = this.cookieService.get('segUserType');
        if (this.userTypeC == 'Customer' && this.userId == ''){
            this.router.navigate(['/home', this.userIdC]);
        }

        if (this.userTypeC == 'Admin'){
            this.api.getOrdersAgg('')
                .subscribe(res => {
                    this.init(res);
                }, err => {
                    console.log(err);
                });
        }
        else{
            this.api.getOrdersAgg(this.userIdC)
                .subscribe(res => {
                    this.init(res);
                }, err => {
                    console.log(err);
                });
        }
    }

    init(res) {

        let dates: string[] = [];
        let customers: string[] = {};
        let series = [];

        res.forEach(function(element) {

            if (!dates[element._id.date]){
                dates.push(element._id.date)
            }
            if (!customers[element._id.userName]){
                customers[element._id.userName] = {
                    "name": element._id.userName,
                    "data": []
                };
            }
            customers[element._id.userName].data.push(element.count);
        });

        for (var customer in customers){

            series.push(customers[customer]);
        }

        let highcharts = Highcharts;
        let chart = new Chart({
        chart: {
            type: 'area',
            inverted: true
        },
        title: {
            text: 'Total Orders'
        },
        subtitle : {
            style: {
                position: 'absolute',
                right: '0px',
                bottom: '10px'
            }
        },
        legend : {
            layout: 'vertical',
            align: 'left',
            verticalAlign: 'top',
            x: -150,
            y: 100,
            floating: true,
            borderWidth: 1,
            backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
        },
        xAxis:{
            categories: dates
        },
        yAxis : {
            title: {
                text: 'Orders'
            },
            labels: {
                formatter: function () {
                    return this.value;
                }
            },
            min:0,
            allowDecimals: false
        },
        tooltip : {
            formatter: function () {
                return '<b>' + this.series.name + '</b><br/>' + this.x + ': ' + this.y;
            }
        },
        plotOptions : {
            area: {
                fillOpacity: 0.5
            }
        },
        credits: {
            enabled: false
        },
        series: series
    });

    this.chart = chart;
    chart.ref$.subscribe(console.log);
  }
}
