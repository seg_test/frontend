import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialog } from '@angular/material'
import { ApiService } from '../api.service';
import { CookieService } from 'ngx-cookie-service';

@Component({

  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {

    constructor(
        private api: ApiService,
        private route: ActivatedRoute,
        private router: Router,
        private cookieService: CookieService) { }

    username: string;

    ngOnInit() {}

    login() : void {
        this.api.getUserLogin(this.username)
            .subscribe(res => {
                console.log(res);
                if(res.length == 1){
                    console.log(res[0]);
                    this.cookieService.set('segUserId', res[0].id);
                    this.cookieService.set('segUserType', res[0].type);
                    //this.router.navigate('/home', element.userId]);
                    if (res[0].type == "Admin"){
                        this.router.navigate(['/home']);
                    }
                    else{
                        this.router.navigate(['/home', res[0].id]);
                    }
                }
                else {
                    alert("Invalid credentials");
                }
            }, err => {
            console.log(err);
        });
    }

 }
