import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})
export class NavigationComponent implements OnInit {

  constructor(private cookieService: CookieService) { }

  userType: string;

  ngOnInit() {

    this.userType = this.cookieService.get('segUserType');
  }

}
