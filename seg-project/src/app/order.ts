export class Order {
    name: string;
    id: number;
    date: string;
    price: number;
    userId: string;
    userName: string;
    mode: string;
}
