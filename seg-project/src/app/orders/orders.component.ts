import { Component, ViewChild, Inject} from '@angular/core';
import { RouterModule, ParamMap, ActivatedRoute, Router } from '@angular/router';
import { MatMenuTrigger, MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ApiService } from '../api.service';
import { Order } from '../order';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.css']
})

export class OrdersComponent{

  constructor(public dialog: MatDialog,
    private api: ApiService,
    private route: ActivatedRoute,
    private router: Router,
    private cookieService: CookieService) { }

  userIdC: string;
  userTypeC: string;
  userId = this.route.snapshot.paramMap.get('userId') || '';
  displayedColumns: string[] = [];
  dataSource: Order[] = [];
  user = {};
  totalUserOrder: number = 8;

  ngOnInit() {

    this.userIdC = this.cookieService.get('segUserId');
    this.userTypeC = this.cookieService.get('segUserType');
    if (this.userTypeC == 'Customer' && this.userId == ''){
        this.router.navigate(['/orders', this.userIdC]);
    }
    if (this.userId == ''){
      this.displayedColumns = ['id', 'userName', 'name', 'date', 'price', 'more'];
    }
    else{
      this.displayedColumns = ['id', 'name', 'date', 'price', 'more'];
    }
    this.api.getUser(this.userId)
      .subscribe(res => {
          this.user = res;
          console.log(this.user);
      }, err => {
          console.log(err);
    });
    this.api.getOrders(this.userId)
      .subscribe(res => {
          this.dataSource = res;
          this.totalUserOrder = res.length;
          console.log(this.dataSource);
      }, err => {
          console.log(err);
      });
  }

  openDialog(item: Order): void {

      const dialogRef = this.dialog.open(OrderDialog, {
          width: '500px',
          data: item
      });

      dialogRef.afterClosed().subscribe(result => {
          console.log('The order dialog was closed');
          if (!result){
            return;
          }
          console.log(result);
          if (result.mode == 'Edit'){
            this.api.updateOrder(result.id, result)
                .subscribe(res => {
                    this.ngOnInit();
                }, err => {
                    console.log(err);
                });
          }
          else if (result.mode == 'Add'){
              var today = new Date();
              var dd = String(today.getDate()).padStart(2, '0');
              var mm = String(today.getMonth() + 1).padStart(2, '0');
              var yyyy = today.getFullYear();
              result.date = yyyy + '-' + mm + '-' + dd;
              console.log(result);
              this.api.addOrder(result)
                  .subscribe(res => {
                      this.ngOnInit();
                  }, err => {
                      console.log(err);
                  });
          }
      });
  }

  @ViewChild(MatMenuTrigger)
  contextMenu: MatMenuTrigger;

  contextMenuPosition = { x: '1000px', y: '1000px' };

  onContextMenu(event: MouseEvent, element: Element) {

       event.preventDefault();
       this.contextMenuPosition.x = event.clientX + 'px';
       this.contextMenuPosition.y = event.clientY + 'px';
       this.contextMenu.menuData = { 'item': element };
       this.contextMenu.openMenu();
   }

   onAdd(){

      console.log(`Click on Add`);
      let item: Order = {};
      item.mode = 'Add';
      item.userId = this.user.id;
      item.userName = this.user.name;
      this.openDialog(item)
   }

   onContextMenuEdit(item: Order) {

      console.log(`Click on Edit for ${item.id}`);
      item.mode = 'Edit';
      this.openDialog(item)
   }

   onContextMenuDelete(item: Order) {

      console.log(`Click on Delete for ${item.id}`);
      this.api.deleteOrder(item.id)
          .subscribe(res => {
              this.ngOnInit();
          }, err => {
              console.log(err);
          });
   }

   onUserSelect(element: Order) {

        this.router.navigate(['/orders', element.userId]);
   }
}

@Component({
  selector: 'order-dialog',
  templateUrl: './order-dialog.html',
})

export class OrderDialog {

    constructor(
        public dialogRef: MatDialogRef<OrderDialog>,
        @Inject(MAT_DIALOG_DATA) public data: Order) {}

    onCancelClick(): void {
        this.dialogRef.close();
    }
}
