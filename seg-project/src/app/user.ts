export class User {
  name: string;
  id: string;
  type: string;
  maxOrders: number;
  creationDate: string;
  mode: string;
}
